# import built-in packages
import os
import sys
import argparse
import multiprocessing
import cPickle as pickle
from collections import defaultdict
sys.path.append("/home/senbong/Tools/hypnos/")  # add path to project root

# import third-party packages
import joblib
import numpy as np
from joblib import Parallel, delayed
from scipy.stats import pearsonr
import matplotlib.pyplot as plt

# import hypnos related packages
import hypnos.benchmark as benchmark
from hypnos.metric import DistanceMetricSuite
from hypnos.selection import DominationSelection, DecompositionSelection,\
        GeometryBasedDecompositionSelection,DecompositionNeighborMixin
from hypnos.metaheuristics import BoxBounder,DifferentialEvolution,\
        SimulatedBinaryXover,PolynomialMutation,OneOffspringVariator
    

# add mixin to create selector with neighbour handle
class DecompositionSelectionWithNeighborHandle(DecompositionSelection, DecompositionNeighborMixin):
    pass


# utility functions used for dynamic multi-objective optimization and inverse modelling
def reevaluate(selector, mop, generation):
    for p in selector.population:
        p.objective = mop.objective_function(p.variable, generation)
    return selector


def random_initialization(selector, mop, generation, r):
    interval = mop.upper_bound - mop.lower_bound
    for p in selector.population:
        if np.random.rand() <= r:
            p.variable = np.random.random(mop.variable_num)*interval + mop.lower_bound
        p.objective = mop.objective_function(p.variable, generation)
    return selector


def reset_ideal(selector):
    selector.ideal_vector = np.array([np.inf]*selector.objective_num)
    for p in selector.population:
        selector._update_reference(p.objective)
    return selector


def inverse_model(objectives, variables):
    F = np.matrix.transpose(objectives)
    X = np.matrix.transpose(variables)
    FF = np.dot(F, objectives)
    if np.linalg.matrix_rank(FF) < objectives.shape[1]:
        FF = FF + 0.01*np.eye(objectives.shape[1])
    return np.dot(X, np.dot(objectives, np.linalg.inv(FF)))


def check_equal(v):
    return np.all(v[0] == v[1:])


def check_pearson(weights, objectives, cutoff=0.7):
    w1, w2 = zip(*weights)
    f1, f2 = zip(*objectives)
    if check_equal(f1) or check_equal(f2):
        return False
    else:
        return np.abs(pearsonr(w1, f1)[0]) > cutoff and np.abs(pearsonr(w2, f2)[0]) > cutoff


def simulation(args):
    mop_name, population_size, interval_num, neighbor_num, max_gen, run, verbose, outdir = args

    import hypnos.benchmark as benchmark
    from hypnos.metric import DistanceMetricSuite
    from hypnos.selection import DominationSelection, DecompositionSelection,\
            GeometryBasedDecompositionSelection, DecompositionNeighborMixin
    from hypnos.metaheuristics import BoxBounder,DifferentialEvolution,\
            SimulatedBinaryXover,PolynomialMutation,OneOffspringVariator

    # add mixin to create selector with neighbour handle
    class DecompositionSelectionWithNeighborHandle(DecompositionSelection, DecompositionNeighborMixin):
        pass

    # start simulation...
    dmetrics = list()
    for r in range(run):
    
        # set random seed
        np.random.seed(r)

        # benchmark problem settings
        print "initialize DMOP and its settings..."
        MOP = getattr(benchmark, mop_name)
        MOP.solution_directory = "/home/senbong/Tools/hypnos/data/"
        mop = MOP()
        mop.reset()
        nobj, nvar = mop.objective_num, mop.variable_num

        # classical NSGA-II, MOEA/D with proposed IM-MOEA/D
        print "initialize selectors for different DMOEAs..."
        selector_domin = DominationSelection(nobj=nobj, population_size=population_size)
        selector_decom = DecompositionSelection(nobj=nobj, interval_num=interval_num, 
                                                neighbor_num=neighbor_num)
        selector_inv   = DecompositionSelectionWithNeighborHandle(nobj=nobj, 
                                                interval_num=interval_num, neighbor_num=neighbor_num)
        selector_geom  = GeometryBasedDecompositionSelection(nobj=nobj, 
                                                interval_num=interval_num, neighbor_num=neighbor_num)

        # meta-heuristic operator settings
        print "initialize meta-heuristic operators..."
        bounder = BoxBounder(method="clip", lower_bound=mop.lower_bound, upper_bound=mop.upper_bound)
        #xover = SimulatedBinaryXover(bounder=bounder, eta_c=20)
        xover = DifferentialEvolution(bounder=bounder)
        mutator = PolynomialMutation(bounder=bounder, eta_m=20, mutation_rate=1.0/nvar)
        variator_domin = OneOffspringVariator(selector=selector_domin, xover=xover, mutator=mutator)
        variator_decom = OneOffspringVariator(selector=selector_decom, xover=xover, mutator=mutator)
        variator_inv  = OneOffspringVariator(selector=selector_inv, xover=xover, mutator=mutator)
        variator_geom = OneOffspringVariator(selector=selector_geom, xover=xover, mutator=mutator)

        # performance metrics settings
        print "initialize performance metrics..."
        dmetric = DistanceMetricSuite(pmetrics=["inverted_generational_distance",
                                                "generational_distance",
                                                "average_hausdorff_distance"])

        # prepare for evolutionary search
        cur_time = mop._generation_to_time(0)
        for generation in range(max_gen):
            for _ in range(selector_domin.population_size):

                if generation == 0:
                    # random initialization (all algorithms have the same initial population)
                    interval = mop.upper_bound - mop.lower_bound
                    variable_domin = np.random.random(nvar)*interval + mop.lower_bound
                    variable_decom = variable_domin
                    variable_inv   = variable_domin
                    variable_geom  = variable_domin
                else:
                    # crossover and mutation
                    print "crossover & mutation..."
                    variable_domin = variator_domin.generate()
                    variable_decom = variator_decom.generate()
                    variable_geom  = variator_geom.generate()

                    # offspring generation for IM-MOEA/D
                    print "inverse modelling..."
                    index = selector_inv.cur_index
                    weights, objectives, variables = selector_inv.get_neighbor(index)
                    if check_pearson(weights, objectives, 0.5):
                        # inverse modeling
                        print "[inverse modelling] inverse matrix..."
                        B = inverse_model(objectives, variables)
                        direction = selector_inv.population[index].objective - selector_inv.ideal_vector
                        direction += 0.01 * np.ones(direction.shape)    # make sure element is not zero

                        print "[inverse modelling] sampling solution..."
                        while True:
                            f_test = selector_inv.population[index].objective + direction*np.random.normal(size=nobj)
                            if not np.all(f_test >= selector_inv.population[index].objective):
                                break

                        variable_inv = bounder(np.dot(B, f_test))
                        variable_inv = bounder(mutator.generate(variable_inv))
                    else:
                        variable_inv = variator_inv.generate()


                # re-evaluate solutions after change is detected
                if mop._generation_to_time(generation) != cur_time:
                    selector_domin = random_initialization(selector_domin, mop, generation, 0.2)
                    selector_decom = random_initialization(selector_decom, mop, generation, 0.2)
                    selector_geom  = random_initialization(selector_geom, mop, generation, 0.2)
                    selector_inv   = random_initialization(selector_inv, mop, generation, 0.2)
                    selector_decom = reset_ideal(selector_decom)
                    selector_geom  = reset_ideal(selector_geom)
                    selector_inv   = reset_ideal(selector_inv)
                    cur_time = mop._generation_to_time(generation)


                # evaluate variable and feed to the selector
                print "feed into selectors..."
                objective = mop.objective_function(variable_domin, generation)
                selector_domin.feed(variable_domin, objective)
                objective = mop.objective_function(variable_decom, generation)
                selector_decom.feed(variable_decom, objective)
                objective = mop.objective_function(variable_inv, generation)
                selector_inv.feed(variable_inv, objective)
                objective = mop.objective_function(variable_geom, generation)
                selector_geom.feed(variable_geom, objective)

            # display message
            if verbose and (generation+1) % max(max_gen//5, 1) == 0:
                print("Generation {:>3}, total fit. evals. {:>5}, total obj. exec. time {:<10}".format(
                        generation+1, mop.fitness_eval_num, mop.total_exec_time))
          
            # calculate performance metrics
            print "calculate performance metrics (generation: {}) ...".format(generation+1)
            objectives = np.array([p.objective for p in selector_domin.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="NSGA2")
            objectives = np.array([p.objective for p in selector_decom.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="MOEA/D")
            objectives = np.array([p.objective for p in selector_inv.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="Inverse MOEAs")
            objectives = np.array([p.objective for p in selector_geom.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="MOEA/D-MRDL")
            
        # save the record for each run
        print "save the record for each run"
        dmetrics.append(dmetric.history)
        
    # output performance metrics
    filename = os.path.join(outdir, "performance_history.pkl")
    joblib.dump(transform_dmetrics(dmetrics), filename)


# convert DistanceMetricSuite format to normal dictionary
def transform_dmetrics(dmetrics):
    if len(dmetrics):
        algo_metric_map = dict()
        for data in dmetrics:
            for metric in data:
                for algo in data[metric]:
                    key = "{}-{}".format(algo, metric)
                    if key not in algo_metric_map:
                        algo_metric_map[key] = [np.array(data[metric][algo])]
                    else:
                        algo_metric_map[key].append(np.array(data[metric][algo]))
        return algo_metric_map


def main():

    # parse the command-line argument
    parser = argparse.ArgumentParser(
            description="Experiment on FDA, GTA problems (FDA1,3, GTA1a-8a, GTA1m-GTA8m)")
    parser.add_argument("-o", "--outdir", help="specify output directory",
            required=True)
    parser.add_argument("-f", "--func_name", help="specify benchmark function", 
            required=True)

    # get the parameters
    args = vars(parser.parse_args())
    
    # run the process in parallel
    arguments = (args["func_name"], 100, 99, 20, 500, 30, True, args["outdir"])
    simulation(arguments)


if __name__ == "__main__":
    main()
