# import built-in packages
import pdb
import os
import argparse
from collections import defaultdict

# import third-party packages
import joblib
import numpy as np


def load_data(indir, problems):
    problem_data_map = dict()
    for p in problems:
        problem_data_map[p] = dict()
        dmetric_filename = os.path.join(indir, p, "performance_history.pkl")
        detection_filename = os.path.join(indir, p, "detection.pkl")
        problem_data_map[p]["dmetric"] = joblib.load(dmetric_filename)
        problem_data_map[p]["detection"] = joblib.load(detection_filename)
    return problem_data_map


def summarize_detection(fevals, dmetric):
    feval_stat = defaultdict(float)
    feval_stat_list = defaultdict(list)
    for f in fevals:
        for k in f:
            feval_stat_list[k].append(f[k])

    for k in f:
        feval_stat[k] = np.mean(feval_stat_list[k])
        feval_stat["{}-std".format(k)] = np.std(feval_stat_list[k])

    # store distance related data
    feval_stat["inverse-igd"] = np.mean(dmetric["Proposed (actual)-inverted_generational_distance"])
    feval_stat["inverse-gd"]  = np.mean(dmetric["Proposed (actual)-generational_distance"])
    feval_stat["ignore-igd"]  = np.mean(dmetric["Ignore Change (actual)-inverted_generational_distance"])
    feval_stat["ignore-gd"]   = np.mean(dmetric["Ignore Change (actual)-generational_distance"])
    feval_stat["fix-igd"]     = np.mean(dmetric["Fix Detector-inverted_generational_distance"])
    feval_stat["fix-gd"]      = np.mean(dmetric["Fix Detector-generational_distance"])

    feval_stat["inverse-igd-std"] = np.std(dmetric["Proposed (actual)-inverted_generational_distance"])
    feval_stat["inverse-gd-std"]  = np.std(dmetric["Proposed (actual)-generational_distance"])
    feval_stat["ignore-igd-std"]  = np.std(dmetric["Ignore Change (actual)-inverted_generational_distance"])
    feval_stat["ignore-gd-std"]   = np.std(dmetric["Ignore Change (actual)-generational_distance"])
    feval_stat["fix-igd-std"]     = np.std(dmetric["Fix Detector-inverted_generational_distance"])
    feval_stat["fix-gd-std"]      = np.std(dmetric["Fix Detector-generational_distance"])
    return feval_stat


def table_arrangement(problems, summarize_data, style, output):
    with open(output, "w") as fhandle:
        column_str = " ".join("c" for _ in problems)
        column_str = "c {}".format(column_str)
        fhandle.write("\documentclass[border=10pt]{standalone}\n")
        fhandle.write("\\begin{document}\n")
        fhandle.write("\minipage{" + str(int(len(problems)/3.5)) + "\\textwidth}\n")
        fhandle.write("\\begin{tabular}{" + column_str + "}\n")
        fhandle.write("{{}} & {}\\\\\n".format(" & ".join(problems)))
        for method in ["ignore", "fix", "inverse"]:
            for m in ["igd", "gd"]:
                row_data = " & ".join(style.format(mean=summarize_data[p]["{}-{}".format(method, m)],
                                                    stdev=summarize_data[p]["{}-{}-std".format(method, m)]) for p in problems)
                fhandle.write("{}-{} & {}\\\\\n".format(method, m, row_data))


        row_data = " & ".join(style.format(mean=summarize_data[p]["ign_rmse"],
                                stdev=summarize_data[p]["ign_rmse-std"]) for p in problems)
        fhandle.write("ign-rmse & {}\\\\\n".format(row_data))
        row_data = " & ".join(style.format(mean=summarize_data[p]["inv_rmse"],
                                stdev=summarize_data[p]["inv_rmse-std"]) for p in problems)
        fhandle.write("inv-rmse & {}\\\\\n".format(row_data))
        row_data = " & ".join("${mean:.2f} \pm {stdev:.2f}$".format(mean=summarize_data[p]["re-evaluation"],
                                stdev=summarize_data[p]["re-evaluation-std"]) for p in problems)
        fhandle.write("re-evaluation & {}\\\\\n".format(row_data))
        row_data = " & ".join("${mean:.2f} \pm {stdev:.2f}$".format(mean=summarize_data[p]["check"],
                                stdev=summarize_data[p]["check-std"]) for p in problems)
        fhandle.write("check & {}\\\\\n".format(row_data))
        fhandle.write("\end{tabular}\n")
        fhandle.write("\endminipage\n")
        fhandle.write("\end{document}")


def extract_table(indir, problems, style, output):
    problem_data_map = load_data(indir, problems)
    summarize_data = dict()
    for p in problem_data_map:
        summarize_data[p] = summarize_detection(problem_data_map[p]["detection"],
                problem_data_map[p]["dmetric"])
    table_arrangement(problems, summarize_data, style, output)


def display(argument): 
    print 80*"="
    print "{:<20} : {}".format("Argument", "Value")
    print 80*"="
    for key,value in argument.items():
        print "{:<20} : {}".format(key, value)
    print 80*"="


def main():

    # parse the command-line argument
    parser = argparse.ArgumentParser(description="Export table in LaTeX format.")
    parser.add_argument("-i", "--indir", help="specify input directory",
            required=True)
    parser.add_argument("-o", "--output", help="specify output file path",
            required=True)
    parser.add_argument("-p", "--problem", nargs="+", help="specify test problem",
            type=str, required=True)
    parser.add_argument("-s", "--style", help="specify pattern style",
            default="${mean:.4f} \pm {stdev:.4f}$")

    # get the parameters
    args = vars(parser.parse_args())
    display(args)

    # extract table in tex format
    extract_table(args["indir"], args["problem"], args["style"], args["output"])


if __name__ == "__main__":
    main()
