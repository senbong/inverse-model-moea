import abc
from collections import deque

import numpy as np


class AbstractChangeDetector(object):
   
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.rules_triggered = False
        self.has_restarted = False
        self.signal_size = 0

    @abc.abstractmethod
    def update_residuals(self, new_signal_value):
        self._update_base_residuals(new_signal_value)

    
    @abc.abstractmethod
    def check_stopping_rules(self, new_signal_value):
        pass
    

    @property
    def residuals_(self):
        return self._get_residual_dict()


    def _update_base_residuals(self, x):
        self.signal_size += 1


    def _get_residual_dict(self):
        residuals_dict = {}
        for k, v in self.__dict__.iteritems():
            if k.endswith('_'):
                residuals_dict[k] = v
        return residuals_dict


    def _step(self, new_signal_value):
        self.has_restarted = True
        self.update_residuals(new_signal_value)
        self.check_stopping_rules(new_signal_value)
        yield self._get_residual_dict()


    def step(self, new_signal_value):
        return self._step(new_signal_value)


    def __repr__(self):
        return "Change Detector(triggered={}, residuals={})".format(self.rules_triggered, self.residuals_)



class ZScoreDetector(AbstractChangeDetector):
    
    def __init__(self, window_size = 100, threshold=0.05):
        super(ZScoreDetector, self).__init__()
        self.threshold = threshold
        self.window_size = window_size
        self.k = 0                              # total signal_size
        self.g_mean = 0.0                       # global mean  
        self.s = 0.0                            # for Welford's method. variance = s/(k + 1)
        self.window = deque(maxlen = window_size)
        self.z_score_ = np.nan
        self.z_array = []

        
    def update_residuals(self, new_signal_value):
        self._update_base_residuals(new_signal_value)
        x = new_signal_value
        self.window.append(x)
        oldm = self.g_mean
        newm = oldm + (x - oldm)/(self.k + 1)
        s = self.s + (x - newm) * (x - oldm)
        
        g_mean = newm                       # Global mean
        g_std = np.sqrt(s/(self.k+1))       # Global std
        w_mean = np.mean(self.window)       # Window mean
        w_std = np.std(self.window)         # Window std
        SE = g_std/np.sqrt(self.window_size)
        
        self.z_score_ = (w_mean - g_mean)/SE if SE else 0.0
        self.z_array.append(self.z_score_)
        self.g_mean = g_mean
        self.s = s
        self.k += 1
    

    def check_stopping_rules(self, new_signal_value): 
        if self.z_score_ > self.threshold:
            self.rules_triggered = True
