# import built-in packages
import pdb
import os
import argparse

# import third-party packages
import joblib
import numpy as np
from scipy.stats import ttest_ind


def load_data(indir, problems):
    problem_data_map = dict()
    for p in problems:
        filename = os.path.join(indir, p, "performance_history.pkl")
        problem_data_map[p] = joblib.load(filename)
    return problem_data_map


def trend_map(problem_data_map):
    mean_trend_map = dict()
    for k1 in problem_data_map:
        mean_trend_map[k1] = dict()
        for k2 in problem_data_map[k1]:
            mean_trend_map[k1][k2] = np.mean(problem_data_map[k1][k2], axis=0)
    return mean_trend_map


def extract_stat_map(problem, keyword, mean_trend_map):
    keys, trends = list(), list()
    for key in mean_trend_map[problem]:
        if keyword in key:
            keys.append(key)
            trends.append(mean_trend_map[problem][key])

            if 'Inverse MOEAs' in key:
                target_trend = mean_trend_map[problem][key]

    stats_map, t_test_map = dict(), dict()
    perf_mean, perf_stdev = np.mean(trends, axis=1), np.std(trends, axis=1)
    for i,key in enumerate(keys):
        stats_map[key]  = perf_mean[i], perf_stdev[i], ttest_ind(target_trend, trends[i], equal_var=False)[1]
        
    ranks = np.argsort(trends, axis=0).argsort(axis=0)
    ranks_mean = ranks[:,1:].mean(axis=1)
    ranks_stdev = ranks[:,1:].std(axis=1)
    rank_map = dict()
    for i,key in enumerate(keys):
        if 'Inverse MOEAs' in key:
            target_rank_trend = ranks[i,:]

    for i,key in enumerate(keys):
        rank_map[key] = ranks_mean[i],  ranks_stdev[i], ttest_ind(target_rank_trend, ranks[i,:], equal_var=False)[1]
    return rank_map, stats_map


def split_key(key):
    tokens = key.split("-")
    algo, metric = "-".join(tokens[:-1]), "-"+tokens[-1]
    return algo, metric


def get_key(keys, algo, metric):
    algo, metric = algo.strip(), metric.strip()
    for key in keys:
        algo_part, _ = split_key(key)
        if algo == algo_part and metric in key:
            return key


def table_arrangement(problems, mean_trend_map, algos, metrics, style, output):
    with open(output, "w") as fhandle:
        column_str = " ".join("c" for _ in problems)
        column_str = "c {}".format(column_str)
        fhandle.write("\documentclass[border=10pt]{standalone}\n")
        fhandle.write("\\begin{document}\n")
        fhandle.write("\minipage{" + str(int(len(problems)/3.0)) + "\\textwidth}\n")
        fhandle.write("\\begin{tabular}{" + column_str + "}\n")
        fhandle.write("{{}} & {}\\\\\n".format(" & ".join(problems)))
        pattern = "{algo}-{metric}-{t}"
        min_map = dict() 
        for metric in metrics:
            for problem in problems:
                ranks_map, stats_map = extract_stat_map(problem, metric.strip(), mean_trend_map)            
                min_map["{}-{}-rank".format(problem, metric)] = np.min([ranks_map[k][0] for k in ranks_map])
                min_map["{}-{}-stat".format(problem, metric)] = np.min([stats_map[k][0] for k in stats_map])


        for algo in algos:
            for metric in metrics:
                ranks, stats = list(), list()
                for problem in problems:
                    min_rank = min_map["{}-{}-rank".format(problem, metric)]
                    min_stat = min_map["{}-{}-stat".format(problem, metric)]
                    ranks_map, stats_map = extract_stat_map(problem, metric.strip(), mean_trend_map)
                    key = get_key(ranks_map.keys(), algo, metric)
                    rsign = '\\textdagger' if ranks_map[key][2] < 0.01 else '' 
                    ssign = '\\textdagger' if stats_map[key][2] < 0.01 else ''
                    rank_str = style.format(mean=ranks_map[key][0], std=ranks_map[key][1], sign=rsign)
                    stat_str = style.format(mean=stats_map[key][0], std=stats_map[key][1], sign=ssign)
                    if ranks_map[key][0] == min_rank:
                        ranks.append("\\textbf{" + rank_str + "}")
                    else:
                        ranks.append(rank_str)

                    if stats_map[key][0] == min_stat:
                        stats.append("\\textbf{" + stat_str + "}")
                    else:
                        stats.append(stat_str)

                first_col = pattern.format(algo=algo.strip(), metric=metric.strip(), t="rank")
                fhandle.write("{} & {}\\\\\n".format(first_col.replace("_", "-"), " & ".join(ranks)))
                first_col = pattern.format(algo=algo.strip(), metric=metric.strip(), t="stats")
                fhandle.write("{} & {}\\\\\n".format(first_col.replace("_", "-"), " & ".join(stats)))

        fhandle.write("\end{tabular}\n")
        fhandle.write("\endminipage\n")
        fhandle.write("\end{document}")


def extract_table(indir, problems, algos, metrics, style, output):
    problem_data_map = load_data(indir, problems)
    mean_trend_map = trend_map(problem_data_map)
    table_arrangement(problems, mean_trend_map, algos, metrics, style, output)


def display(argument):
    print 80*"="
    print "{:<20} : {}".format("Argument", "Value")
    print 80*"="
    for key,value in argument.items():
        print "{:<20} : {}".format(key, value)
    print 80*"="


def main():

    # parse the command-line argument
    parser = argparse.ArgumentParser(description="Export table in LaTeX format.")
    parser.add_argument("-i", "--indir", help="specify input directory",
            required=True)
    parser.add_argument("-o", "--output", help="specify output file path",
            required=True)
    parser.add_argument("-a", "--algo", nargs="+", help="specify algorithm key",
            type=str, required=True)
    parser.add_argument("-m", "--metric", nargs="+", help="specify metric key",
            type=str, required=True)
    parser.add_argument("-p", "--problem", nargs="+", help="specify test problem",
            type=str, required=True)
    parser.add_argument("-s", "--style", help="specify pattern style",
            default="{mean}$\pm${std}")

    # get the parameters
    args = vars(parser.parse_args())
    display(args)

    # extract table in tex format
    extract_table(args["indir"], args["problem"], args["algo"], 
                    args["metric"], args["style"], args["output"])


if __name__ == "__main__":
    main()
