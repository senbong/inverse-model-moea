# import built-in packages
import pdb
import os
import sys
import argparse
import multiprocessing
import cPickle as pickle
from collections import defaultdict
sys.path.append("/home/senbong/Tools/hypnos/")  # add path to project root

# import third-party packages
import joblib
import numpy as np
from joblib import Parallel, delayed
from scipy.stats import pearsonr
import matplotlib.pyplot as plt

# import hypnos related packages
import hypnos.benchmark as benchmark
from hypnos.metric import DistanceMetricSuite
from hypnos.selection import DominationSelection, DecompositionSelection,\
        GeometryBasedDecompositionSelection,DecompositionNeighborMixin
from hypnos.metaheuristics import BoxBounder,DifferentialEvolution,\
        SimulatedBinaryXover,PolynomialMutation,OneOffspringVariator
    
# import change detector
from change_detector import *


# add mixin to create selector with neighbour handle
class DecompositionSelectionWithNeighborHandle(DecompositionSelection, DecompositionNeighborMixin):
    pass


# utility functions used for dynamic multi-objective optimization and inverse modelling
def reevaluate(selector, mop, generation):
    for p in selector.population:
        p.objective = mop.objective_function(p.variable, generation)
    return selector


def random_initialization(selector, mop, generation, r):
    interval = mop.upper_bound - mop.lower_bound
    for p in selector.population:
        if np.random.rand() <= r:
            p.variable = np.random.random(mop.variable_num)*interval + mop.lower_bound
        p.objective = mop.objective_function(p.variable, generation)
    return selector


def reset_ideal(selector):
    selector.ideal_vector = np.array([np.inf]*selector.objective_num)
    for p in selector.population:
        selector._update_reference(p.objective)
    return selector


def inverse_model(objectives, variables):
    F = np.matrix.transpose(objectives)
    X = np.matrix.transpose(variables)
    FF = np.dot(F, objectives)
    if np.linalg.matrix_rank(FF) < objectives.shape[1]:
        FF = FF + 0.01*np.eye(objectives.shape[1])
    return np.dot(X, np.dot(objectives, np.linalg.inv(FF)))


def check_equal(v):
    return np.all(v[0] == v[1:])


def check_pearson(weights, objectives, cutoff=0.7):
    w1, w2 = zip(*weights)
    f1, f2 = zip(*objectives)
    if check_equal(f1) or check_equal(f2):
        return False
    else:
        return np.abs(pearsonr(w1, f1)[0]) > cutoff and np.abs(pearsonr(w2, f2)[0]) > cutoff


def rmse(objs_left, objs_right):
    assert(objs_left.shape == objs_right.shape)
    rmses = list()
    for obj1, obj2 in zip(objs_left, objs_right):
        rmses.append(np.sqrt(((obj1 - obj2)**2).mean()))
    return np.mean(rmses)


def generate_inverse_model_offspring(selector_inv, bounder, mutator, variator_inv):
    index = selector_inv.cur_index
    weights, objectives, variables = selector_inv.get_neighbor(index)
    if check_pearson(weights, objectives, 0.5):
        # inverse modeling
        B = inverse_model(objectives, variables)
        direction = selector_inv.population[index].objective - selector_inv.ideal_vector
        direction += 0.01 * np.ones(direction.shape)    # make sure element is not zero
        # sampling
        while True:
            f_test = selector_inv.population[index].objective + direction*np.random.normal(size=selector_inv.objective_num)
            if not np.all(f_test >= selector_inv.population[index].objective):
                break

        variable_inv = bounder(np.dot(B, f_test))
        variable_inv = bounder(mutator.generate(variable_inv))
    else:
        f_test       = np.inf
        variable_inv = variator_inv.generate()
    return variable_inv, f_test


def simulation(args):
    mop_name, population_size, interval_num, neighbor_num, max_gen, run, verbose, outdir = args

    import hypnos.benchmark as benchmark
    from hypnos.metric import DistanceMetricSuite
    from hypnos.selection import DominationSelection, DecompositionSelection,\
            GeometryBasedDecompositionSelection, DecompositionNeighborMixin
    from hypnos.metaheuristics import BoxBounder,DifferentialEvolution,\
            SimulatedBinaryXover,PolynomialMutation,OneOffspringVariator

    # add mixin to create selector with neighbour handle
    class DecompositionSelectionWithNeighborHandle(DecompositionSelection, DecompositionNeighborMixin):
        pass

    # start simulation...
    dmetrics = list()
    fevals = list()
    for r in range(run):
    
        # set random seed
        np.random.seed(r)

        # benchmark problem settings
        print "initialize DMOP and its settings..."
        MOP = getattr(benchmark, mop_name)
        MOP.solution_directory = "/home/senbong/Tools/hypnos/data/"
        mop = MOP()
        mop.reset()
        nobj, nvar = mop.objective_num, mop.variable_num

        # classical NSGA-II, MOEA/D with proposed IM-MOEA/D
        print "initialize selectors for different DMOEAs..."
        selector_inv_fix = DecompositionSelectionWithNeighborHandle(nobj=nobj, 
                                                interval_num=interval_num, neighbor_num=neighbor_num)
        selector_inv_ign = DecompositionSelectionWithNeighborHandle(nobj=nobj, 
                                                interval_num=interval_num, neighbor_num=neighbor_num)
        selector_inv     = DecompositionSelectionWithNeighborHandle(nobj=nobj, 
                                                interval_num=interval_num, neighbor_num=neighbor_num)

        # meta-heuristic operator settings
        print "initialize meta-heuristic operators..."
        bounder = BoxBounder(method="clip", lower_bound=mop.lower_bound, upper_bound=mop.upper_bound)
        #xover = SimulatedBinaryXover(bounder=bounder, eta_c=20)
        xover = DifferentialEvolution(bounder=bounder)
        mutator = PolynomialMutation(bounder=bounder, eta_m=20, mutation_rate=1.0/nvar)
        variator_inv_fix = OneOffspringVariator(selector=selector_inv_fix, xover=xover, mutator=mutator)
        variator_inv_ign = OneOffspringVariator(selector=selector_inv_ign, xover=xover, mutator=mutator)
        variator_inv     = OneOffspringVariator(selector=selector_inv, xover=xover, mutator=mutator)

        # performance metrics settings
        print "initialize performance metrics..."
        dmetric = DistanceMetricSuite(pmetrics=["inverted_generational_distance",
                                                "generational_distance",
                                                "average_hausdorff_distance"])

        # detection mechanism
        window_size, threshold = 100, 1
        detector = ZScoreDetector(window_size=window_size, threshold=threshold)

        # detection performance
        feval = defaultdict(int)

        # prepare for evolutionary search
        cur_time_idx, last_trigger_idx = 0, 0
        cur_time = mop._generation_to_time(0)
        inv_rmse, ign_rmse = list(), list()
        for generation in range(max_gen):
            random_indices = list(range(selector_inv.population_size))
            for _ in range(selector_inv_fix.population_size):

                if generation == 0:
                    # random initialization (all algorithms have the same initial population)
                    interval = mop.upper_bound - mop.lower_bound
                    variable_inv_fix = np.random.random(nvar)*interval + mop.lower_bound
                    variable_inv_ign = variable_inv_fix
                    variable_inv     = variable_inv_fix
                else:
                    # offspring generation for IM-MOEA/D
                    random_indices.remove(selector_inv.cur_index)
                    variable_inv_fix, _ = generate_inverse_model_offspring(
                            selector_inv_fix, bounder, mutator, variator_inv_fix)
                    variable_inv_ign, _ = generate_inverse_model_offspring(
                            selector_inv_ign, bounder, mutator, variator_inv_ign)
                    variable_inv, f_test = generate_inverse_model_offspring(
                            selector_inv, bounder, mutator, variator_inv)

                # re-evaluate solutions after change is detected
                if mop._generation_to_time(generation) != cur_time:
                    print("Actual Fitness Landscape changes at generation {}".format(generation))
                    selector_inv_fix = random_initialization(selector_inv_fix, mop, generation, 0.2)
                    selector_inv_fix = reset_ideal(selector_inv_fix)
                    cur_time = mop._generation_to_time(generation)

                # evaluate variable and feed to the selector
                objective_inv_fix = mop.objective_function(variable_inv_fix, generation)
                selector_inv_fix.feed(variable_inv_fix, objective_inv_fix)
                objective_inv_ign = mop.objective_function(variable_inv_ign, generation)
                selector_inv_ign.feed(variable_inv_ign, objective_inv_ign)
                objective_inv = mop.objective_function(variable_inv, generation)
                selector_inv.feed(variable_inv, objective_inv)

                # check modelling error & change detection
                if generation != 0 and np.all(f_test != np.inf) and len(random_indices):
                    # first-stage change detection
                    error = np.linalg.norm(f_test - objective_inv)
                    next(detector.step(error))
                    if detector.rules_triggered:
                        #print("Trigger first-stage detection at generation {}...".format(generation))
                        # first check the interval between last triggered event & current time index
                        if cur_time_idx - last_trigger_idx > selector_inv.population_size:
                            print("Trigger second-stage detection at generation {}...".format(generation))
                            last_trigger_idx = cur_time_idx
                            rand_idx = np.random.choice(random_indices)
                            temp_objective = mop.objective_function(selector_inv.population[rand_idx].variable, generation)
                            feval["check"] += 1
                            if not np.all(selector_inv.population[rand_idx].objective == temp_objective):
                                selector_inv = reevaluate(selector_inv, mop, generation)
                                selector_inv = reset_ideal(selector_inv)
                                feval["re-evaluation"] += selector_inv.population_size
                                cur_time_idx += selector_inv.population_size
                                detector = ZScoreDetector(window_size=window_size, threshold=threshold)
                                print("Detect change in generation {}...".format(generation))

                # update current time index
                cur_time_idx += 1

            # display message
            if verbose and (generation+1) % max(max_gen//5, 1) == 0:
                print("Generation {:>3}, total fit. evals. {:>5}, total obj. exec. time {:<10}".format(
                        generation+1, mop.fitness_eval_num, mop.total_exec_time))
          
            # calculate performance metrics
            objectives = np.array([p.objective for p in selector_inv_fix.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="Fix Detector")

            objectives = np.array([p.objective for p in selector_inv_ign.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="Ignore Change")
            objectives_actual = np.array([mop.objective_function(p.variable, generation) 
                                            for p in selector_inv_ign.population])
            dmetric.update(mop.pareto_front(generation), objectives_actual, 
                            marker="Ignore Change (actual)")
            ign_rmse.append(rmse(objectives, objectives_actual))

            objectives = np.array([p.objective for p in selector_inv.population])
            dmetric.update(mop.pareto_front(generation), objectives, marker="Proposed")
            objectives_actual = np.array([mop.objective_function(p.variable, generation) 
                                            for p in selector_inv.population])
            dmetric.update(mop.pareto_front(generation), objectives_actual, 
                            marker="Proposed (actual)")
            inv_rmse.append(rmse(objectives, objectives_actual))
            
            
        # save the record for each run
        print "save the record for each run"
        dmetrics.append(dmetric.history)
        feval["inv_rmse"] = np.mean(inv_rmse)
        feval["ign_rmse"] = np.mean(ign_rmse)
        fevals.append(feval)
        
    # output performance metrics
    filename = os.path.join(outdir, "performance_history.pkl")
    joblib.dump(transform_dmetrics(dmetrics), filename)
    filename = os.path.join(outdir, "detection.pkl")
    joblib.dump(fevals, filename)


# convert DistanceMetricSuite format to normal dictionary
def transform_dmetrics(dmetrics):
    if len(dmetrics):
        algo_metric_map = dict()
        for data in dmetrics:
            for metric in data:
                for algo in data[metric]:
                    key = "{}-{}".format(algo, metric)
                    if key not in algo_metric_map:
                        algo_metric_map[key] = [np.array(data[metric][algo])]
                    else:
                        algo_metric_map[key].append(np.array(data[metric][algo]))
        return algo_metric_map


def main():

    # parse the command-line argument
    parser = argparse.ArgumentParser(description="Experiment on ZDT, CEC-09 problems (ZDT1-4,6, UF1-7)")
    parser.add_argument("-o", "--outdir", help="specify output directory",
            required=True)
    parser.add_argument("-f", "--func_name", help="specify benchmark function", 
            required=True)

    # get the parameters
    args = vars(parser.parse_args())
    
    # run the process in parallel
    arguments = (args["func_name"], 100, 99, 20, 500, 30, True, args["outdir"])
    simulation(arguments)


if __name__ == "__main__":
    main()
