PYTHON = python
CEC09 = UF1 UF2 UF3 UF4 UF5 UF6 UF7 
ZDT = ZDT1 ZDT2 ZDT3 ZDT4 ZDT6
WFG = WFG1 WFG2 WFG3 WFG4 WFG5 WFG6 WFG7 WFG8 WFG9
STATIC_BENCHMARK = $(CEC09) $(ZDT) $(WFG)
FDA = FDA1 FDA3
GTAa = GTA1a GTA2a GTA3a GTA4a GTA5a GTA6a GTA7a GTA8a
GTAm = GTA1m GTA2m GTA3m GTA4m GTA5m GTA6m GTA7m GTA8m
DYNAMIC_BENCHMARK = $(FDA) $(GTAa) $(GTAm)
DETECTION = $(addprefix det, $(DYNAMIC_BENCHMARK))

static: $(STATIC_BENCHMARK)

$(STATIC_BENCHMARK): static_inverse_modelling.py
	$(PYTHON) $< -o output/$@ -f $@

$(DYNAMIC_BENCHMARK): dynamic_inverse_modelling.py
	$(PYTHON) $< -o output/$@ -f $@

$(DETECTION): change_detection.py
	$(PYTHON) $< -o output_detection/$(@:det%=%) -f $(@:det%=%)

static_table: export_table.py
	$(PYTHON) $< -i output -p $(STATIC_BENCHMARK) -a "NSGA2" "MOEA/D" "MOEA/D-MRDL" "Inverse MOEAs" -m "inverted_" " -generational_" " -average" -b 10 -g 20 -e 50 -o tex/table.tex -s "{mean:<.4f} $$\pm$$ {std:<.4f} {sign}"

static_table_classic: export_table.py
	$(PYTHON) $< -i output -p $(STATIC_BENCHMARK) -a "NSGA2" "MOEA/D" "Inverse MOEAs" -m "inverted_" " -generational_" -b 10 -g 20 -e 50 -o tex/table_classic.tex -s "{mean:<.4f} $$\pm$$ {std:<.4f} {sign}"

dynamic_table: export_dynamic_table.py
	$(PYTHON) $< -i output -p $(DYNAMIC_BENCHMARK) -a "NSGA2" "MOEA/D" "MOEA/D-MRDL" "Inverse MOEAs" -m "inverted_" " -generational_" " -average" -o tex/dynamic_table.tex -s "{mean:<.4f} $$\pm$$ {std:<.4f} {sign}"

dynamic_table_classic: export_dynamic_table.py
	$(PYTHON) $< -i output -p $(DYNAMIC_BENCHMARK) -a "NSGA2" "MOEA/D" "Inverse MOEAs" -m "inverted_" " -generational_" " -average" -o tex/dynamic_table_classic.tex -s "{mean:<.4f} $$\pm$$ {std:<.4f} {sign}"

detection_table: export_detection_table.py
	$(PYTHON) $< -i output_detection -p $(DYNAMIC_BENCHMARK) -o tex/detection_table_classic.tex -s "{mean:<.3f} $$\pm$$ {stdev:<.3f}"

.PHONY: static $(STATIC_BENCHMARK)
