# import built-in packages
import pdb
import os
import argparse

# import third-party packages
import joblib
import numpy as np
from scipy.stats import ttest_ind


def load_data(indir, problems):
    problem_data_map = dict()
    for p in problems:
        filename = os.path.join(indir, p, "performance_history.pkl")
        problem_data_map[p] = joblib.load(filename)
    return problem_data_map


def stats_map(data_map):
    mean_map, stdev_map = dict(), dict()
    for p in data_map:
        mean_map[p], stdev_map[p] = dict(), dict()
        for key in data_map[p]:
            mean_map[p][key] = np.mean(data_map[p][key], axis=0)
            stdev_map[p][key] = np.std(data_map[p][key], axis=0)
    return mean_map, stdev_map


def significant_map(data_map):
    t_test_map, target_map = dict(), dict()
    for p in data_map:
        target_map[p] = dict()
        for key in data_map[p]:
            algo, metric = split_key(key)
            if algo == 'Inverse MOEAs':
                target_map[p][metric] = data_map[p][key]

    for p in data_map:
        t_test_map[p] = dict()
        for key in data_map[p]:
            algo, metric = split_key(key)
            matrix_a = np.array(data_map[p][key])
            matrix_b = np.array(target_map[p][metric])
            p_values = [ttest_ind(matrix_a[:,i], matrix_b[:,i], axis=0, equal_var=False)[1]
                        for i in range(matrix_a.shape[1])]
            t_test_map[p][key] = p_values
    return t_test_map


def split_key(key):
    tokens = key.split("-")
    algo, metric = "-".join(tokens[:-1]), "-"+tokens[-1]
    return algo, metric


def get_key(keys, algo, metric):
    algo, metric = algo.strip(), metric.strip()
    for key in keys:
        algo_part, _ = split_key(key)
        if algo == algo_part and metric in key:
            return key


def get_min_flag(mean_map, metric, intervals, means, algos):
    mean_matrix = list()
    metric = metric.strip()
    algos = [a.strip() for a in algos]
    for key in mean_map:
        algo_part, _ = split_key(key)
        for algo in algos:
            if metric in key and algo == algo_part:
                mean_matrix.append([mean_map[key][g-1] for g in intervals])
    min_mean = np.min(mean_matrix, axis=0)
    return means == min_mean


def table_arrangement(problems, mean_map, stdev_map, smap, algos, metrics, 
        gen_start, gen_int, gen_end, style, output, alpha=0.01):
    with open(output, "w") as fhandle:
        intervals = range(gen_start, gen_end+1, gen_int)
        column_str = " ".join("c" for _ in intervals)
        column_str = "c {}".format(column_str)
        fhandle.write("\documentclass[border=10pt]{standalone}\n")
        fhandle.write("\\begin{document}\n")
        fhandle.write("\minipage{" + str(max(int((gen_end - gen_start)/(3.6*gen_int)),2)) + "\\textwidth}\n")
        fhandle.write("\\begin{tabular}{" + column_str + "}\n")
        pattern = "{algo}-{metric}-{problem}"
        gen_str = " & ".join("gen={}".format(g) for g in intervals)
        fhandle.write("key & {}\\\\\n".format(gen_str))
        fhandle.write("\hline\n")
        for problem in problems:
            for algo in algos:
                for metric in metrics:
                    first_col = pattern.format(algo=algo, problem=problem, metric=metric).replace("_", "-")
                    key = get_key(mean_map[problem].keys(), algo, metric)
                    means = [mean_map[problem][key][g-1] for g in intervals]
                    stdevs = [stdev_map[problem][key][g-1] for g in intervals]
                    signs = [smap[problem][key][g-1] < alpha for g in intervals]
                    min_flag = get_min_flag(mean_map[problem], metric, intervals, means, algos)
                    items = list()
                    for i,(m,s,n) in enumerate(zip(means, stdevs, signs)):
                        sign = '\\textdagger' if n else ''
                        elem = style.format(mean=m, std=s, sign=sign)
                        if min_flag[i]:
                            items.append("\\textbf{" + elem + "}")
                        else:
                            items.append(elem)
                    fhandle.write("{}\\\\\n".format(first_col + " & " +  " & ".join(items)))
            fhandle.write("\hline\n")
        fhandle.write("\end{tabular}\n")
        fhandle.write("\endminipage\n")
        fhandle.write("\end{document}")


def extract_table(indir, problems, algos, metrics, gen_start, gen_int, gen_end, style, output):
    problem_data_map = load_data(indir, problems)
    mean_map, stdev_map = stats_map(problem_data_map)
    smap = significant_map(problem_data_map)
    table_arrangement(problems, mean_map, stdev_map, smap, algos, metrics, 
            gen_start, gen_int, gen_end, style, output)


def display(argument):
    print 80*"="
    print "{:<20} : {}".format("Argument", "Value")
    print 80*"="
    for key,value in argument.items():
        print "{:<20} : {}".format(key, value)
    print 80*"="


def main():

    # parse the command-line argument
    parser = argparse.ArgumentParser(description="Export table in LaTeX format.")
    parser.add_argument("-i", "--indir", help="specify input directory",
            required=True)
    parser.add_argument("-o", "--output", help="specify output file path",
            required=True)
    parser.add_argument("-a", "--algo", nargs="+", help="specify algorithm key",
            type=str, required=True)
    parser.add_argument("-m", "--metric", nargs="+", help="specify metric key",
            type=str, required=True)
    parser.add_argument("-p", "--problem", nargs="+", help="specify test problem",
            type=str, required=True)
    parser.add_argument("-s", "--style", help="specify pattern style",
            default="{mean}$\pm${std}")
    parser.add_argument("-b", "--gen_begin", help="specify starting generation",
            type=int, required=True)
    parser.add_argument("-g", "--gen_int", help="specify generational interval",
            type=int, required=True)
    parser.add_argument("-e", "--gen_end", help="specify end generation number",
            type=int, required=True)

    # get the parameters
    args = vars(parser.parse_args())
    display(args)

    # extract table in tex format
    extract_table(args["indir"], args["problem"], args["algo"], 
                    args["metric"], args["gen_begin"], args["gen_int"], args["gen_end"], 
                    args["style"], args["output"])


if __name__ == "__main__":
    main()
